function layThongTinTuForm() {
  const tkNV = document.getElementById("tknv").value;
  const tenNV = document.getElementById("name").value;
  const emailNV = document.getElementById("email").value;
  const matKhauNV = document.getElementById("password").value;
  const ngayLamNV = document.getElementById("datepicker").value;
  const luongCBNV = document.getElementById("luongCB").value;
  const chucVuNV = document.getElementById("chucvu").value;
  const gioLamNV = document.getElementById("gioLam").value;

  return new NhanVien(
    tkNV,
    tenNV,
    emailNV,
    matKhauNV,
    ngayLamNV,
    luongCBNV,
    chucVuNV,
    gioLamNV
  );
}

//render array nhân viên ra giao diện

function renderDSNV(arr) {
  var str = "";
  var n = arr.length;

  for (var i = 0; i < n; i++) {
    var nv = arr[i];
    str += `<tr>
    <td>${nv.taikhoan}</td>
    <td>${nv.hoten}</td>
    <td>${nv.email}</td>
    <td>${nv.ngaylam}</td>
    <td>${nv.chucvu}</td>
    <td>${nv.tongluong()}</td>
    <td>${nv.xeploai()}</td>
    <td>
      <button class="btn btn-primary" onclick="xoaNhanVien('${
        nv.taikhoan
      }')">Xóa</button>
      <button class="btn btn-success" data-toggle="modal"
      data-target="#myModal" onclick="suaNhanVien('${
        nv.taikhoan
      }')">Sửa</button>
    </td>
    </tr>`;
  }
  document.getElementById("tableDanhSach").innerHTML = str;
}

function timKiemViTri(id, dsnv) {
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i].taikhoan == id) {
      return i;
    }
  }
  return -1;
}

function showThongTinRaForm(nv) {
  document.getElementById("tknv").value = nv.taikhoan;
  document.getElementById("name").value = nv.hoten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matkhau;
  document.getElementById("datepicker").value = nv.ngaylam;
  document.getElementById("luongCB").value = nv.luongcoban;
  document.getElementById("chucvu").value = nv.chucvu;
  document.getElementById("gioLam").value = nv.giolam;
}
function timNhanVienTheoLoai(txtLoai, arr) {
  var n = arr.length;
  var arrLoai = [];
  for (var i = 0; i < n; i++) {
    if (arr[i].xeploai() == txtLoai) {
      arrLoai.push(arr[i]);
    }
  }
  return arrLoai;
}
