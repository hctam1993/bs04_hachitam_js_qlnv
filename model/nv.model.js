function NhanVien(
  taikhoan,
  hoten,
  email,
  matkhau,
  ngaylam,
  luongcoban,
  chucvu,
  giolam
) {
  this.taikhoan = taikhoan;
  this.hoten = hoten;
  this.email = email;
  this.matkhau = matkhau;
  this.ngaylam = ngaylam;
  this.luongcoban = luongcoban;
  this.chucvu = chucvu;
  this.giolam = giolam;

  this.tongluong = function () {
    var tongLuong = 0;
    if (this.chucvu == "Sếp") {
      tongLuong = this.luongcoban * 3;
    } else if (this.chucvu == "Trưởng phòng") {
      tongLuong = this.luongcoban * 2;
    } else {
      tongLuong = this.luongcoban * 1;
    }
    return tongLuong;
  };
  this.xeploai = function () {
    var loai = "";
    if (this.giolam * 1 >= 192) {
      loai = "Xuất sắc";
    } else if (this.giolam * 1 >= 176) {
      loai = "Giỏi";
    } else if (this.giolam * 1 >= 160) {
      loai = "Khá";
    } else {
      loai = "Trung bình";
    }
    return loai;
  };
}
