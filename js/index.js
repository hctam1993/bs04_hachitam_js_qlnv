const DSNV_LOCALSTORGE = "DSNV_LOCALSTORGE";

var dsnv = [];

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORGE);

if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);
  var n = dsnv.length;
  for (var i = 0; i < n; i++) {
    var nv = dsnv[i];
    dsnv[i] = new NhanVien(
      nv.taikhoan,
      nv.hoten,
      nv.email,
      nv.matkhau,
      nv.ngaylam,
      nv.luongcoban,
      nv.chucvu,
      nv.giolam
    );
  }
  renderDSNV(dsnv);
}

function themNV() {
  var newNV = layThongTinTuForm();
  var arrClass = document.querySelectorAll(".sp-thongbao");
  console.log("arrClass: ", arrClass);
  arrClass.forEach(function (item) {
    item.style.display = "block";
  });

  var isvalidTK =
    validator.kiemTraRong(
      newNV.taikhoan,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newNV.taikhoan,
      "tbTKNV",
      "Tài khoản từ 4-6 kí tự",
      4,
      6
    );

  var isvalidTen =
    validator.kiemTraRong(newNV.hoten, "tbTen", "Họ tên không được để trống") &&
    validator.kiemTraChiChu(newNV.hoten, "tbTen", "Họ tên chỉ có chữ");

  var isvalidEmail =
    validator.kiemTraRong(
      newNV.email,
      "tbEmail",
      "Email không được để trống"
    ) && validator.kiemTraEmail(newNV.email, "tbEmail", "Nhập đúng kiểu email");

  var isvalidPassword =
    validator.kiemTraRong(
      newNV.matkhau,
      "tbMatKhau",
      "Mật khẩu không được để trống"
    ) &&
    validator.kiemTraPassword(
      newNV.matkhau,
      "tbMatKhau",
      "Mật khẩu phải 6-10 kí tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  var isvalidNgayLam =
    validator.kiemTraRong(
      newNV.ngaylam,
      "tbNgay",
      "Ngày làm không được để trống"
    ) &&
    validator.kiemTraNgay(
      newNV.ngaylam,
      "tbNgay",
      "Ngày phải đúng dịnh dạng dd/MM/yyyy"
    );
  var isvalidLuong =
    validator.kiemTraRong(
      newNV.luongcoban,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) &&
    validator.kiemTraLuong(
      newNV.luongcoban,
      "tbLuongCB",
      "Lương cơ bản 1.000.000 - 20.000.000"
    );
  var isvalidChucVu = validator.kiemTraChucVu(
    newNV.chucvu,
    "tbChucVu",
    "Chức vụ phải hợp lệ"
  );
  var isvalidGioLam =
    validator.kiemTraRong(
      newNV.giolam,
      "tbGiolam",
      "Giờ làm không được để trống"
    ) &&
    validator.kiemTraSoGioLam(
      newNV.giolam,
      "tbGiolam",
      "Số giờ làm trong tháng 80 - 200 giờ"
    );

  if (
    isvalidTK &&
    isvalidTen &&
    isvalidEmail &&
    isvalidPassword &&
    isvalidNgayLam &&
    isvalidLuong &&
    isvalidChucVu &&
    isvalidGioLam
  ) {
    dsnv.push(newNV);
    //   console.log("dsnv", dsnv);

    // tạo json
    var dsnvJson = JSON.stringify(dsnv);
    // lưu json vào localstorge
    localStorage.setItem(DSNV_LOCALSTORGE, dsnvJson);

    renderDSNV(dsnv);
  }
}

function xoaNhanVien(id) {
  var index = timKiemViTri(id, dsnv);

  if (index != -1) {
    dsnv.splice(index, 1);
    renderDSNV(dsnv);
  }
}
var index = -1;
function suaNhanVien(id) {
  index = timKiemViTri(id, dsnv);
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;
  if (index != -1) {
    showThongTinRaForm(dsnv[index]);
  }
}
function capNhatNV() {
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnThemNV").disabled = false;
  var arrClass = document.querySelectorAll(".sp-thongbao");
  console.log("arrClass: ", arrClass);
  arrClass.forEach(function (item) {
    item.style.display = "block";
  });
  var cnTen = document.getElementById("name").value;
  var cnEmail = document.getElementById("email").value;
  var cnPass = document.getElementById("password").value;
  var cnNgay = document.getElementById("datepicker").value;
  var cnLuong = document.getElementById("luongCB").value;
  var cnChucVu = document.getElementById("chucvu").value;
  var cnGioLam = document.getElementById("gioLam").value;
  if (index >= 0) {
    var isvalidTen =
      validator.kiemTraRong(cnTen, "tbTen", "Họ tên không được để trống") &&
      validator.kiemTraChiChu(cnTen, "tbTen", "Họ tên chỉ có chữ");

    var isvalidEmail =
      validator.kiemTraRong(cnEmail, "tbEmail", "Email không được để trống") &&
      validator.kiemTraEmail(cnEmail, "tbEmail", "Nhập đúng kiểu email");

    var isvalidPassword =
      validator.kiemTraRong(
        cnPass,
        "tbMatKhau",
        "Mật khẩu không được để trống"
      ) &&
      validator.kiemTraPassword(
        cnPass,
        "tbMatKhau",
        "Mật khẩu phải 6-10 kí tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
      );
    var isvalidNgayLam =
      validator.kiemTraRong(cnNgay, "tbNgay", "Ngày làm không được để trống") &&
      validator.kiemTraNgay(
        cnNgay,
        "tbNgay",
        "Ngày phải đúng dịnh dạng dd/MM/yyyy"
      );
    var isvalidLuong =
      validator.kiemTraRong(
        cnLuong,
        "tbLuongCB",
        "Lương cơ bản không được để trống"
      ) &&
      validator.kiemTraLuong(
        cnLuong,
        "tbLuongCB",
        "Lương cơ bản 1.000.000 - 20.000.000"
      );
    var isvalidChucVu = validator.kiemTraChucVu(
      cnChucVu,
      "tbChucVu",
      "Chức vụ phải hợp lệ"
    );
    var isvalidGioLam =
      validator.kiemTraRong(
        cnGioLam,
        "tbGiolam",
        "Giờ làm không được để trống"
      ) &&
      validator.kiemTraSoGioLam(
        cnGioLam,
        "tbGiolam",
        "Số giờ làm trong tháng 80 - 200 giờ"
      );

    if (
      isvalidTen &&
      isvalidEmail &&
      isvalidPassword &&
      isvalidNgayLam &&
      isvalidLuong &&
      isvalidChucVu &&
      isvalidGioLam
    ) {
      dsnv[index].hoten = cnTen;
      dsnv[index].email = cnEmail;
      dsnv[index].matkhau = cnPass;
      dsnv[index].ngaylam = cnNgay;
      dsnv[index].luongcoban = cnLuong;
      dsnv[index].chucvu = cnChucVu;
      dsnv[index].giolam = cnGioLam;

      // tạo json
      var dsnvJson = JSON.stringify(dsnv);
      // lưu json vào localstorge
      localStorage.setItem(DSNV_LOCALSTORGE, dsnvJson);

      renderDSNV(dsnv);

      document.getElementById("tknv").value = "";
      document.getElementById("name").value = "";
      document.getElementById("email").value = "";
      document.getElementById("password").value = "";
      document.getElementById("datepicker").value = "";
      document.getElementById("luongCB").value = "";
      document.getElementById("chucvu").value = "";
      document.getElementById("gioLam").value = "";
    }
  }
}
function timNV() {
  var loai = document.getElementById("searchName").value;
  var arrLoai = timNhanVienTheoLoai(loai, dsnv);
  renderDSNV(arrLoai);
}
